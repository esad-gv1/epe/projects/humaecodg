# Choisir son papier

## Les Écolabels

### Relatifs à l’origine de la fibre&nbsp;: PEFC vs FSC

#### FSC (Forest Stewardship Council)
<p class="subdivision">Créé en 1993 par un groupe d’organisations venant de 25 pays. Ce label a développé des critères et des principes liés à une bonne gestion forestière. Son attribution implique un audit et est soutenu par des ONG.  On peut trouver les entreprises pouvant être des fournisseurs ou des acheteurs de bois certifié FSC, vous pouvez chercher par numéro de certification, type de certificat, pays, nom de compagnie sur le site du FSC.
Les critères de son attribution se fondent sur 10 principes environnementaux et sociaux relatifs à la gestion des forêts&nbsp;:</p>

<p class="cit">- Respects des lois et des principes FSC<br>
- Propriété foncière et droit d’usage<br>
- Droit des peuples indigènes<br>
- Relations communautaires et droits des travailleurs<br>
- Bonne gestion forestière<br>
- Minimisation de l’impact environnemental (eau, sols, biodiversité…)<br>
- Formalisation d’un plan de gestion<br>
- Suivi et évaluation des opérations<br>
- Conservation des forêts protégées<br> 
- Planification des plantation</p>


#### PEFC
<p class="subdivision">Créé en 1999 par le Conseil PEFC (Program Endorsement of Forest Certification), c’est une organisation internationale qui valorise une gestion durable des forêts. En France, c’est une adhésion volontaire avec contribution financière et engagement à appliquer les recommandations de la politique régionale en termes de gestion forestière. Son attribution se base sur le respect des normes minimales fondées sur les 6 recommandations définies lors des conférences d’Helsinki en 1993 et de Lisbonne en 1998. Ces normes sont:</p>

<p class="cit">- Conservation et amélioration appropriée des ressources forestières<br>
- Maintien de la santé des écosystèmes forestiers<br>
- Maintien et encouragement des fonctions de production des forêts<br>
- Maintien, conservation et amélioration appropriée de la diversité biologique dans les écosystèmes forestiers<br>
- Maintien et amélioration appropriée des fonctions de protection par la gestion des forêts (sols et eaux)<br>
- Maintien des autres fonction socio-économiques</p>

À noter que ce label n’est pas mentionné dans la liste de recommandations de l’ADEME.

#### PEFC vs FSC
<p class="subdivision">Ces deux labels concernent donc les papiers pour lesquels la gestion des forêts desquelles il est issu, se veut durable. Cela représente moins de 10% des forêts mondiales. Il faut savoir que le label FSC certifie un engagement et une pratique déjà concrétisés tandis que le PEFC demande un engagement d’amélioration. Ce dernier est donc des plus controversé et n’est pas soutenu par bon nombre d’ONG comme GreenPeace ou WWF qui regrettent une distribution peu regardante. En France, les forêts publiques sont gérées par l’Office National de Forêts (ONF), 100% des forêts domaniales et 57% des forêts communales sont labellisées PEFC contre seulement 4 forêts labellisées FSC. Les forêts publiques en France ne représentent que 25% des forêts, le reste étant privé… En comparant, les cartes des deux labels, on constate que les forêts PEFC sont bien plus nombreuses sur le globe, outre-Atlantique, Europe et Asie. Tandis que les forêts FSC se trouvent majoritairement en Amérique du Nord, au Brésil, en Russie et en Suède et Pologne pour l’Europe.</p>
<p class="subdivision">En conclusion, pour avoir une réelle certification de la gestion des forêts desquelles vient le papier que l’on choisit il est préférable d’opter pour le label FSC en priorité. Pour suivre dans l’idée de réduire l’impact environnemental induit par des logistiques de transports le choix de la Suède ou bien de la Pologne permet d’avoir plus de proximité que le restant des pays.</p>

### Relatifs aux produits finis

#### L’Écolabel européen
<p class="subdivision">Créé en 1992, il permet de s’assurer que les impacts sur l’environnement sur tout le cycle de vie du produit est limité. Ses exigences sont relatives aux émissions dans l’eau et dans l’air, à la consommation d’énergie, à la gestion durable des forêts, aux substances chimiques dangereuses et à la gestion des déchets.</p>
<p class="subdivision">Sa gestion se fait par un organisme désigné par l’État et son développement se fait sur les principes de&nbsp;:<br>
- crédibilité, sur base d’études scientifiques et de consultation d’acteurs concernés (ONG, industriels, associations…)<br>
- fiabilité, certification effectuée par une tierce partie<br>
- visibilité, logo reconnu<br>
- sélectivité, seuls 10 à 20% des produits d’une catégorie peuvent prétendre à avoir ce label</p>
<p class="subdivision">Son attribution repose sur 4 critère&nbsp;: l’information, l’écologie, la santé et la performance.</p>

#### Ange Bleu
<p class="subdivision">Le label Ange Bleu est l’écolabel officiel allemand, créé en 1977. Il s’agit de l’écolabel le plus ancien, administré par un jury d’experts. Son approche est globale est concerne le cycle de vie du produit. Il garantit que le papier a été produit à partir de papier de récupération (papier 100 % recyclé), que les substances dangereuses pour la santé ou ayant un impact sur l’écotoxicité aquatique sont interdites ou limitées lors de la fabrication et que les émissions de composés organiques volatils (COV) sont limitées.</p>

#### Nordic Swan - Le Cygne Blanc
<p class="subdivision">Créé en 1989, par la Norvège et la Suède, il garantit que le papier contient des fibres de bois issues de forêts gérées durablement (FSC,PEFC…) pour les fibres vierges (au moins 30 %), ou des fibres recyclées (au moins 75 %), ou un mélange des deux. Il est délivré pour une durée de 3 ans et est composé par divers représentants de professionnels, d’ONG, de l’Agence de protection de l’environnement et de consommateurs.</p>

#### La boucle de Mobius
<p class="subdivision">Très connue, la boucle de Mobius n’est pas un label mais une autodéclaration sans aucune certification par une tierce partie. C’est le symbole international du recyclage, créé en Allemagne dans les années 1970. Elle indique donc la recyclabilité d’un produit (tous les produits papetiers courant). Lorsqu’elle est accompagnée d’un pourcentage, il indique la proportion de matière recyclée dans le papier.</p>

#### APUR
<p class="subdivision">Créé en 1992 par l’ Association des Producteurs et Utilisateurs de Papiers Recyclés, ce label promeut l’usage de papier recyclé tout en donnant une garantie aux utilisateurs sur la quantité de fibres recyclées. Son attribution se base sur le taux de fibre recyclée qui doit être de 50% minimum. Un numéro d’agrément est déclaré de façon à permettre l’identification et la traçabilité de son pays d’origine, de son fabricant etc. Cependant, ce label n’exige pas la non utilisation de chlore, c’est pourquoi il n’est pas recommandé par l’ADEME.</p>

## Recyclabilité 

Les Écolabels sont un des prismes avec lequel on peut choisir son papier en connaissances de causes cependant il y a aussi le prisme de la recyclabilité.
<p class="subdivision">En effet, malgré les écolabels certains papiers sont très difficiles à recycler notamment en fonction de la substances déposer sur leur surface.</p> 
<p class="subdivision">Les papiers ayant subi une transformation telle que la plastification, ou un traitement de résistance à la lumière ne sont pas recyclables. Ces papiers inclut donc les papiers autocollants et les papiers photos.  Le prisme de la recyclabilité inclut donc le fait de réfléchir à l’après consommation de sa production.</p>

## Ancrage local

La France est un pays avec de nombreux papetiers à travers son territoire. Choisir un papier français, c’est réduire l’impact environnemental dû à son transport, par voie routière, maritime ou aérienne. 
Sur le site de Copacel, l’union française des industrie des cartons, papiers et celluloses, une carte interactive du territoire est disponible avec les principaux fabricants nationaux. 
Cette carte est légendée par type de papier et support&nbsp;: les cartons plats, le papier journal, le papier pour emballage souple, le papier d’impression-écriture, le papier ondulé, le papier industriel et spécial, le papier d’hygiène et la pâte de cellulose. 

Il semblerait donc que pour choisir un papier, ces trois paramètres soient intéressants à prendre en compte. Comme pour la conception graphique, on ne peut pas vraiment standardiser le choix d’un papier on peut cependant choisir en fonction des particularités et des besoins d’un projet afin de trouver un équilibre entre ces trois critères.
