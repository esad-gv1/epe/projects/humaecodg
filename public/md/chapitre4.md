# Choisir son hébergeur

## Introduction

L’hébergement web, constitue la mise à disposition d’un espace de stockage afin de publier son site internet. L’utilisateur saisit un nom de domaine qui permet d’afficher le contenu associé. Ce contenu est stocké sur les serveurs de l’hébergeur, et se trouvent dans un datacenter.
<p class="subdivision">Comme nous avons pu le voir auparavant, l’industrie des datacenters constitue une part importante de l’impact de l’industrie web, la consommation de ressources électriques pour maintenir le fonctionnement 24h/24. C’est l’énergie nécessaire au bon fonctionnement de tout le système qui consomme.</p>

## L’hébergement Vert, qu’est-ce que c’est ?

Un hébergement vert est un hébergeur dont les datacenters sont alimentés avec des ressources respectueuses de l’environnement, soit des énergies renouvelables.
<p class="subdivision">Le challenge principal de l’hébergement vert consiste à éviter la climatisation des datacenters. La solution la plus simple étant de constituer un parc de serveurs qui supportent une température à 35°C et qui peuvent se refroidir un utilisant l’air extérieur filtré.</p>

## The Green Web Foundation

La Green Web Foundation œuvre pour un Internet sans énergie fossile d'ici 2030. Nous maintenons le plus grand ensemble de données ouvertes au monde de sites Web fonctionnant à l'énergie verte. Nous proposons des outils open source pour gérer l’impact environnemental des services numériques. Et nous aidons les dirigeants du mouvement et les décideurs politiques à encadrer efficacement le débat et à plaider en faveur d’un Internet durable et juste.
<p class="subdivision">Parmi les outils proposés par la Green Web Foundation se trouve, l’annuaire des hébergeurs verts. On peut y naviguer en sélectionnant notre pays, pour plus de proximité. Le répertoire trier par ordre alphabétique apparaît en détaillant certains aspect de chaque hébergeur tels que le fait de savoir s’il s’agit de serveurs physique ou virtuels, s’il propose un hébergement partagé pour les sites web ou s’il inclus la gestion de Wordpress ou non.</p>
<p class="subdivision">Au cours des différents projets explorés, nous avons vu que les Éditions Wildproject utilisent les serveurs d’AlwaysData. Cependant en allant sur leur site internet, on ne trouve que très peu d’informations relatives à leurs engagements.</p>

## Digitalforest

Mon choix s’est tourné vers Digital Forest, leur data center ainsi que leurs centres de stockage décentralisés sont tous alimentés à 100% en énergie renouvelable.
<p class="subdivision">Cette énergie est issue de barrage hydroélectrique qui se trouve dans les Alpes française, à Grenoble précisément.</p> 
