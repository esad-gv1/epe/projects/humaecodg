# Gestion de l'encre

## Impression en quadrichromie

Lors d’une impression en quadrichromie, les quatre couleurs CMJN sont mélangées par superposition les unes sur les autres afin de créer des nuances supplémentaires et ainsi donner une large gamme de couleurs.
<p class="subdivision">Il existe plusieurs paramètres dans la gestion de l’encre tels que la charge d’encre, la couverture et le taux d’encrage. La charge d’encre définit la quantité d’encre utilisée à l’aplat et dépend du procédé d’impression ainsi que du support utilisé. La couverture d’encre définit la surface d’encre choisie et varie en fonction de la composition graphique.</p>

## Le taux d'encrage

L’indicateur qui nous intéresse ici est le taux d’encrage, il s’agit de l’addition des pourcentages utilisés pour chaque couleur CMJN. En impression, le taux d’encrage maximal est de 300%, et 270% pour un papier recyclé. Le taux d’encrage influence aussi le choix du papier, avec un taux d’encrage à 300%, il faudra utilisé un papier suffisamment épais pour éviter tout problème de séchage. Par conséquent, un taux d’encrage inférieur permet de réduire sa consommation de papier en utilisant un papier plus fin. Les logiciels de traitement graphique nous indiquent le taux d’encrage.

Sylvain Boyer, designer français, a créé le « Guide Ecobranding CMJN», c’est un outil de couleur open source qui présente 167 quadrichromies CMJN avec leurs valeurs correspondantes. Ces couleurs limitent la consommation d’encre utilisée lors de l’impression. On parle alors de la notion d’éco-encrage, pour laquelle les couleurs utilisées ont un taux d’encrage ne dépassant pas les 100%.

Du côté de la couverture d’encre, et donc de la composition graphique il y a des paramètres sur lesquels nous pouvons agir afin de limiter les zones d’aplats tels que le fait de favoriser les zones claires en donnant plus de contraste par exemple. Si un aplat est se trouve indispensable, il est conseillé de favoriser un ton direct, ce dernier aura un taux d’encrage à 100 % pour un résultat équivalent. 
<p class="subdivision">Le guide CITEO partage un calcul du poids de l’encre économisé et son bénéfice environnemental en équivalence de CO2 ou bien en Km.</p>
<p class="subdivision">Cependant un outil de calcul en ligne a été développé pour nous faciliter le travail. Le calculateur BEE, Bilan Environnemental des Emballages, permet d’effectuer le calcul tout en le partageant lors d’un travail collaboratif.</p>