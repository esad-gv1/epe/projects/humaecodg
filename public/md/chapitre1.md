# Écrire son code, aller à l’essentiel&nbsp;!

## L'écriture

Lors de l’écriture de code numérique, il est important d’avoir à l’esprit d’écrire uniquement ce qui est nécessaire au bon fonctionnement de sa page web. De façon simple, ajouter des lignes, ajoute du poids au fichiers. 
<p class="subdivision">En développement web on peut entendre parler de « minifier son code » ou de « minification », le premier intérêt étant d’alléger le poids de son fichier, de sa page web et d’en optimiser le temps de chargement. La minification d’un code peut s’appliquer à des fichiers de divers langage informatiques, fichiers HTML, JavaScript, CSS.</p>


## La minification, en quoi ça consiste&nbsp;? 

Minifier son code se traduit par la suppression de tous les éléments qui ne sont pas utiles à l’éxécution du code par une machine. Les développeurs, étant humains, intègrent souvent des sauts de lignes ainsi que des commentaires afin de rendre le code plus lisible, aéré et ainsi permettre une maintenance ultérieure plus facile. Ces éléments vont prendre une certaine place et par conséquent alourdir le poids de la page. Cependant on parle aussi « d’obfuscation » du code, soit l’obscurcir, car en effet un code minifié devient illisible et incompréhensible par un humain, ce qui rend la maintenance par un tiers impossible. C’est pourquoi, les codes minifiés constituent uniquement les fichiers mis en ligne. Conserver une copie du code non-minifié est nécessaire pour actualiser, débugger et maintenir une page web actuelle. C’est cette copie qui sera minifiée à nouveau.

Les transformations appliquées sont diverses, comme vu précédemment, il y a la suppression des commentaires, des caractères vides mais il peut aussi y avoir l’abréviation de noms de variables pour les fichiers JavaScript ainsi qu'un regroupement des fichiers en fonction de leur type. Ce dernier permet de réduire le nombre de requêtes ; chaque fichier qui constitue une page internet, envoie une requête http(s) au serveur, qui devra trouver le fichier pour ensuite l’envoyer et pour finir le navigateur devra télécharger ce contenu. Tant de fichiers donneront tant de requêtes et donc autant d’aller-retour.

<p class="subdivision">C’est une solution connue du monde du développement néanmoins le fait qu’il soit nécessaire d’avoir une copie de son code lisible stockée pour pouvoir assurer la maintenance en fait une solution valable uniquement d’un point de vue de l’optimisation informatique mais pas d’un point de vue écologique. On peut cependant retenir le fait de regrouper les fichiers pour limiter les requêtes envoyées.</p>

## Site statique vs dynamique&nbsp;?

Un site web statique est construit en langage HTML, CSS et Javascript. 
Un site web dynamique inclus des langages supplémentaires qui permettent de charger des contenus différents à chaque utilisateurs (comme des publicités personnalisés ou le scroll infini, par exemple). À chaque connexion, un site dynamique enverra des requêtes serveur afin de charger le contenu adéquat. 
<p class="subdivision">Tandis que du côté d’un site web statique, les fichiers sources sont stockés et pré-générés. Par conséquent, un site statique se chargera plus rapidement et nécessitera moins de traitement, et donc moins d’énergie pour apparaître.</p>

## CMS&nbsp;? 

Les CMS (Contents Managements System), sont des outils de gestion de contenus, tels que Wordpress. Ils permettent aussi de créer un site grâce des templates déjà fournis. 
<p class="subdivision">Ces CMS sont souvent accompagnés de toute une base de données nécessaires à leur fonctionnement permettant de nombreuses fonctionnalités pour un site internet, y compris certaines qui ne sont pas utilisées pour ce site précis.</p>
<p class="subdivision">Leur utilisation n’est donc pas des plus sobres en terme énergétique. Cependant dans le cadre d’un projet, il se peut que l’actualisation des contenus du site web se fassent par une personne non-initiée au code et donc il est très utile pour qu’une personne puisse gérer elle-même son site. </p>

Il existe des CMS éco-conçus tels que Kirby ou Translucide. Ce dernier est un collectif principalement nantais et français ayant créé un CMS du même nom. Leur CMS est conçu avec les fonctionnalités de base nécessaires au fonctionnement d’un site, chaque nouvelle fonctionnalité ajoutée fait l’objet d’une discussion entre les membres du collectif.

