# Calculer le poids d'une page web 

## httparchive.org

Sur le site web httparchive.org on peut avoir accès à toutes sortes d’informations concernant la construction ainsi que l’évolution du web. Ces archives font l’état des lieux du web.
<p class="subdivision">On peut alors observer l’évolution du poids médian d’une page web depuis 2011. Ce dernier est passé de 518,9 Ko sur ordinateur de bureau en 2011 à 24445Ko en octobre 2023. De la même façon, sur mobile, le poids média d’une page était de 144,8Ko contre 2174Ko en octobre 2023.</p> 
<p class="subdivision">Pour calculer le poids de sa propre page internet, il existe différents outils. Dans un premier temps, on peut profiter des outils dédiés à l’optimisation information sous un prisme d’optimisation écologique.</p>

## Les outils 

webpagetest.org, est un outil connu de la communauté informatique afin d’améliorer les temps de chargement. Il permet d’obtenir le poids d’une page web et détaille par la même occasion le nombre de requêtes envoyées par la page. Il évalue tous les éléments et établi la liste de ceux qui pèsent le plus lourd.

De cette façon, je peux donc savoir que la page d’accueil de mon site professionnel pèse 5,597Ko, que mes images envoient la moitié des requêtes et constituent la majeure partie du poids totale de la page. 
De plus, Webpage test donne des éléments d’amélioration en expliquant qu’avec une bonne gestion et un bon format d’image, la compression pourrait se faire à hauteur de 48%, ce qui n’est pas négligeable. Il renvoie sur une site d’analyse d’images&nbsp;: webspeedtest.cloudinary.com, qui conseille les différents formats possibles ainsi que leur réduction de poids respective.
Aujourd’hui, le format AVIF a l’air d’être le plus léger, suivi de près par le format WEBP mais nous reviendront là-dessus sur un point dédié au traitements des images. 

Dans un second temps, des outils d’optimisation écologiques sont aussi développés comme&nbsp;: 

websitecarbon.com est un outil qui permet de calculer les émissions de CO2 générées par une page web. Il permet ainsi de se situer parmi l’immensité du nombre de site présents sur internet. De cette façon, j’apprends que la page d’accueil de mon site est 84% plus « sale » que les autres sites, ce qui fait de mon portfolio un très mauvais candidat. (En cours de refonte)
Le rapport généré se base sur approximativement 10000 visites par mois, ce qui est discutable, afin de donner des équivalences comme le nombre de tasse de thé consommées ou le nombre d’arbres nécessaires à l’absorption des émissions de carbone. Enfin, il suggère trois plan d’actions possible comme&nbsp;: changer d’hébergeur et en prendre un qui utilise des énergies renouvelable, faire en sorte que son site soit plus efficient et pour finir planter des arbres. Pour toutes autres informations, il faut basculer sur un modèle payant étant donné qu’il s’agit d’un cabinet de conseil.

Ecograder, est un outil qui a pour vocation d’aider les gens à mieux comprendre, suivre, gérer et réduire leur empreinte numérique. Développé par Mightbytes, une agence digitale focalisée sur l’impact environnemental, se trouvant à Chicago, la première version fut publiée le jour de le Terre 2013, soit le 22 avril et la dernière en 2023. 
Cet outil donne un rapport assez détaillé sur les éléments à améliorer, ils se trouvent catégorisés en trois points&nbsp;: le poids des pages, la conception UX et l’hébergement vert, ces derniers étant eux-mêmes détaillés et divisés en sous-parties. Par défaut, le rapport se base sur un nombre de visites de 1000 par mois, mais ce dernier est ajustable. Ecograder est conçu pour prioriser les actions que vous pouvez entreprendre afin de réduire l’impact de votre site et renvoie également à des ressources supplémentaires pour des sujets plus approfondis. 