import { loadScripts } from "./js/utils.js";

//pagedjs reserved iframe to rendrer the paginated version
const pagedjsFrame = document.getElementById("paged-preview-frame");
const previewPrintBt = document.getElementById("previewPrintBt");
const printBt = document.getElementById("printBt");

let elementsToPaginate = [];

let selectionStarted = false;

//additional script to load, not importable as es modules
const scritpList = [
    "vendors/js/markdown-it.js",
    "vendors/js/markdown-it-footnote.js",
    "vendors/js/markdown-it-attrs.js",
    "vendors/js/markdown-it-container.js"
];

//sync batch loading
await loadScripts(scritpList);

//markdown files to load
const mdFilesList = [
    "md/chapitre1.md",
    "md/chapitre2.md",
    "md/chapitre3.md",
    "md/chapitre4.md",
    "md/chapitre5.md",
    "md/chapitre6.md",
    "md/chapitre7.md"
];

//markdownit instanciation (old school method as no ES6 modules are available)
const markdownit = window.markdownit
    ({
        // Options for markdownit
        langPrefix: 'language-en',
        // You can use html markup element
        html: true,
        typographer: true,
        // Replace english quotation by french quotation
        quotes: ['«\xA0', '\xA0»', '‹\xA0', '\xA0›'],
    })
    .use(markdownitContainer) //div
    .use(markdownItAttrs, { //custom html element attributes
        // optional, these are default options
        leftDelimiter: '{',
        rightDelimiter: '}',
        allowedAttributes: [] // empty array = all attributes are allowed
    });

const menuColumn = document.getElementById("menu");
const selectionColumn = document.getElementById("selection");
const selectionColumnContent = document.getElementById("selection-content");
const chapitresColumn = document.getElementById("chapitres");

//function to produce the HTML from md files
async function layoutHTML() {
    //Handles close and print bt separately

    //!important! forEach can't be used as it doesn't respect await order!
    for (let index = 0; index < mdFilesList.length; index++) {

        const mdFile = mdFilesList[index];
        const reponse = await fetch(mdFile);
        const mdContent = await reponse.text();
        //convertion from md to html, returns a string
        const result = markdownit.render(mdContent);

        //bluid chapitres div containers
        const chapitre = document.createElement("div");
        chapitre.className = "chapitre";
        chapitre.id = "chapitre-" + index;
        if (index !== 0) {
            chapitre.style.display = "none";
        }
        //gets real HTML Nodes from string rendered by Markdownit
        const elementsFromParser = new DOMParser().parseFromString(result, "text/html");
        //console.log([...elementsFromParser.body.children], "elements");
        const AllChildren = getAllChildren(elementsFromParser.body);
        AllChildren.forEach((element) => {
            element.dataset.active = true;
        })

        const HTMLElements = [...elementsFromParser.body.children];

        HTMLElements.forEach((element) => {
            //console.log("adding", element);
            chapitre.appendChild(element);
        });

        chapitresColumn.appendChild(chapitre);

        //generates menu from md titles
        const title = chapitre.getElementsByTagName("H1")[0];
        //console.log(title);
        if (title) {
            //give new title and id to connect to chapters
            title.id = "title-" + index;
            title.className = "title";
            const titleClone = title.cloneNode(true);

            menuColumn.appendChild(titleClone);

            //add interaction on each title inside the menu
            titleClone.addEventListener("click", titlesClickHandler);
        }

        //interaction on chapitres
        chapitresColumn.addEventListener("click", onInteractChapiters);
    };

    //pagedjsFrame.style.display = "none";

    //closebt
    /* closeBt = document.getElementById("closeBt");
    closeBt.style.display = "none";
    closeBt.addEventListener("click", (event) => {
        pagedJsPreviewDiv.innerHTML = "";
        pagedJsPreviewDiv.style.display = "none";
        closeBt.style.display = "none";
    }); */
}

function titlesClickHandler(event) {
    const title = event.target;

    //gestion titre au click
    const allTitles = [...title.parentElement.children];
    allTitles.forEach( (oneTitle) => {
        oneTitle.style.color = "black";
    });
    title.style.color = "rgb(242, 172, 60)";

    //gestion des chapitres
    const chapitreToGet = title.id.split("-")[1];
    console.log("chapitresColumn", chapitresColumn);
    const chapitre = document.getElementById("chapitre-" + chapitreToGet);

    //hide every chapitre
    [...chapitresColumn.children].forEach((chapitre) => {
        chapitre.style.display = "none";
    });
    //show the one we clicked on
    chapitre.style.display = "block";
}

function onInteractChapiters(event) {

    let srcElement = event.srcElement;
    const srcTagName = srcElement.tagName.toLowerCase();

    //click has been done on something not clickable = stop it now!
    if (srcElement.dataset.active === undefined) {
        return;
    };

    //escape if the element is already in the selection
    const isActive = JSON.parse(srcElement.dataset.active);
    if (isActive === false) {
        console.log("srcElement.dataset.active = ", srcElement.dataset.active);
        return;
    }
    //console.log("srcElement.dataset.active = ", srcElement.dataset.active);

    //let the selection column appear!
    if (!selectionStarted) {
        selectionColumn.style.flexBasis = "200%";
        selectionStarted = true;
    }

    //temporary memory to cumulate elements to paginate afterward
    const toMemory = [];
    /**
     * click on p to get upper level h and grab all its content
     */
    if (srcTagName.indexOf("p") === 0 && srcTagName.length === 1) {
        //got one of h1 h2 h3 etc, manage it with the dedicated function
        const selection = managePElements(srcElement);
        if (selection) {
            const headingElementFromSelection = selection[0];
            const canBeAdded = JSON.parse(headingElementFromSelection.dataset.active);
            if (canBeAdded) {
                selectionColumnContent.appendChild(selection[0].cloneNode(true));
            }
            selection.map((el) => {
                toMemory.push(el);
            });
        }
    }

    /**
    * click H -> get next H
    *   no H => prendre tout jusqu'à la fin
    *   H => prendre tout jusqu'au H suivant
    */
    if (srcTagName.indexOf("h") === 0 && srcTagName.length === 2) {
        //got one of h1 h2 h3 etc, manage it with the dedicated function
        const selection = manageHeadingElement(srcElement, srcTagName);
        if (selection) {
            const headingElementFromSelection = selection[0];
            const canBeAdded = JSON.parse(headingElementFromSelection.dataset.active);
            if (canBeAdded) {
                selectionColumnContent.appendChild(selection[0].cloneNode(true));
            }
            selection.map((el) => {
                toMemory.push(el);
            });
        }
    }

    console.log("toMemory", toMemory);

    toMemory.forEach((el) => {
        //deactivate the element for future selection
        const isActive = JSON.parse(el.dataset.active);
        if (isActive === true) {
            elementsToPaginate.push(el.cloneNode(true));
        }
        el.dataset.active = false;
        el.classList.add('selected-typography'); /////////////////// C'EST LÀÀÀÀÀÀÀ !!!!!!!!!!!!!!!!!!
    });
    console.log(elementsToPaginate);

}

/**
 * click H1 -> get next H1
 *   no H1 => prendre tout jusqu'à la fin
 *   H1 => prendre tout jusqu'au H1 suivant
 * 
 * click H2 -> get next H2
 *   no H2 => prendre tout jusqu'à la fin
 *   H2 => prendre tout jusqu'au H2 suivant
 * 
 * click H3 -> get next H3
 *   no H3 => prendre tout jusqu'à la fin
 *   H3 => prendre tout jusqu'au H3 suivant
 * 
 * click H4 -> get next H4
 *   no H4 => prendre tout jusqu'à la fin
 *   H4 => prendre tout jusqu'au H4 suivant
 * 
 * click p -> get previous H + get next H but exclude it from selection
 * 
 */

function manageHeadingElement(element, tagName) {
    const parentDiv = element.parentElement;//chaptire parent (avec tous les enfants)
    const chapitreChildren = [...parentDiv.children];//Array contenant les enfants à plat

    //index du 1er H concerné dans la liste des enfants
    const startIndex = chapitreChildren.indexOf(element);
    let endIndex = null;

    let tag = null;
    if (tagName === "h1") {
        tag = "h1";
    }
    else if (tagName === "h2") {
        tag = "h2";
    }
    else if (tagName === "h3") {
        tag = "h3";
    }
    else if (tagName === "h4") {
        tag = "h4";
    }

    const hLevel = Number(tag.substring(1, tag.length));

    const chapitreChildrenExtract = chapitreChildren.slice(startIndex, chapitreChildren.length);
    for (let i = 1; i < chapitreChildrenExtract.length; i++) {
        const child = chapitreChildrenExtract[i];
        const childTagName = child.tagName.toLowerCase();
        if (childTagName.indexOf("h") >= 0) {
            //got a h!
            const childHLevel = Number(childTagName.substring(1, tag.length));
            if (childHLevel === hLevel || childHLevel < hLevel) {
                endIndex = chapitreChildren.indexOf(child);
                //endIndex -= 1;
                break;
            }
        }
        else {
            //on a pas de H qui suit, on prend le dernier enfant de la liste
            endIndex = chapitreChildren.length;
        }
    }
    const selection = chapitreChildren.slice(startIndex, endIndex);
    //console.log(startIndex, chapitreChildren[startIndex], endIndex, chapitreChildren[endIndex]);
    return selection;
}

/**
 * gestion des éléments P
 * Trouve l'élèment h précédent le plus proche et le transmet à manageHeadingElement
**/
function managePElements(element) {
    const parentDiv = element.parentElement;//chaptire parent (avec tous les enfants)
    const chapitreChildren = [...parentDiv.children];//Array contenant les enfants à plat

    const pIndex = chapitreChildren.indexOf(element);

    for (let i = pIndex; i >= 0; i--) {
        const child = chapitreChildren[i];
        const childTagName = child.tagName.toLowerCase();
        if (childTagName.indexOf("h") >= 0) {
            console.log(child, childTagName);
            return manageHeadingElement(child, childTagName);
        }
    }
    return null;
}


function getAllParents(element) {
    const parents = [];
    let parent = element.parentElement;

    while (parent) {
        parents.push(parent);
        parent = parent.parentElement;
    }

    return parents;
}

function getAllChildren(element) {
    var children = [];
    var child = element.firstElementChild;

    while (child) {
        children.push(child);
        if (child.firstElementChild) {
            children = children.concat(getAllChildren(child));
        }
        child = child.nextElementSibling;
    }

    return children;
}

//wait to have all the element loaded (module scripts can't be defered)
window.addEventListener("load", async (event) => {
    await layoutHTML();
});

//interaction
previewPrintBt.addEventListener("click", () => {
    pagedjsFrame.style.display = "block";
    const serializedElements = elementsToPaginate.map(element => element.outerHTML);
    pagedjsFrame.contentWindow.postMessage(JSON.stringify(serializedElements), '*');
    /*  console.log("erase memory");
     elementsToPaginate = []; */
});

//message reception
window.addEventListener("message", async (event) => {
    console.log("message", event.source, event.data);
    if (event.data) {
        if (event.data === "closeMe") {
            pagedjsFrame.style.display = "none";
        }
    }
});

//print paged.js paginated content
printBt.addEventListener("click", (event) => {
    pagedjsFrame.contentWindow.print();
});
