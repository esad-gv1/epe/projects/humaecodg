# Choisir son imprimeur

## Les labels 

#### Norme - ISO 14001
<p class="subdivision">La norme ISO 14001 est un standard international qui a pour principe la recherche d’amélioration continue en 4 temps&nbsp;: planifier, developper, contrôler et améliorer.</p>
<p class="subdivision">Chez les imprimeurs comme n’importe quelle entreprise, il s’agit d’une démarche de management environnemental qui vise à améliorer la performance environnementale d’un site en maîtrisant ses impacts sur son milieu. Cette norme est révisée régulièrement afin d’en maintenir sa pertinence. À chaque mises à jour, les entreprises ont 3 ans pour se mettre en conformité.</p>

#### Label Print’Ethic
<p class="subdivision">Créé en 2018 et élaboré avec l’AFNOR par l’Union nationales des industries de l’impression et de la communication (UNIIC). Ce label est aujourd’hui géré par l’Institut de Développement et d’Expertises Plurimédia (IDEP). Ce label de responsabilité sociale des entreprises (RSE) est entièrement dédié aux industries graphiques, il se base sur 12 enjeux&nbsp;:</p>

1. Définir la politique de responsabilité sociétale de l’entreprise, la piloter grâce à une organisation pérenne.
2. Définir une stratégie de positionnement et de développement de l’entreprise à 3/5 ans.
3. Intégrer une culture de l’innovation, dans les produits, les services, l’organisation de l’entreprise.
4. Identifier les parties prenantes prioritaires de l’entreprise et dialoguer avec elles.
5. Favoriser le développement d’un dialogue social de qualité.
6. Identifier les risques de l’entreprise et prendre des mesures de prévention.
7. Investir dans les compétences des salariés, élément-clé de compétitivité et de sécurisation des parcours professionnels.
8. S’impliquer pour améliorer la connaissance et l’attractivité du métier et pour former des professionnels qualifiés.
9. Réduire l’impact environnemental de la production à travers notamment une optimisation du volume et de la qualité des matières premières utilisées.
10. Réduire les émissions de gaz à effet de serre et la consommation d’énergie.
11. Mettre en œuvre le Règlement Européen sur la protection des données.
12. Promouvoir la RSE chez les fournisseurs et sous-traitants.»

Le label est divisé en 4 niveaux différents dont les entreprises membres ont l’obligation de monter d’un niveau tous les 3 ans. Pour obtenir un label de niveau 4, cela peut donc prendre 12 ans. Il s’agit là d’une volonté de mettre en place un processus de transformation globale. 


#### Imprim’Vert
<p class="subdivision">Imprim’Vert est une marque collective créé en 1998 afin de responsabiliser les sites de production de l’imprimerie. Matthieu Prévost, animateur national de la marque et responsable environnement au sein de l’UNIIC, explique les 5 critères nécessaires&nbsp;: « l’élimination conforme des déchets dangereux. Parmi les autres, on trouve&nbsp;: la sécurisation des stockages de liquides dangereux, la non-utilisation de produits CMR (Cancérogènes, Mutagènes et Toxiques), la sensibilisation environnementale des salariés et de la clientèle et, enfin, le suivi des consommations énergétiques du site.» Néanmoins, du côté de l’ADEME cette marque est considérée comme une garantie minimale uniquement quant à la gestion globale des déchets et produits dangereux.</p>

## Relation à l’imprimeur - Ancrage local

Pour la production d’un objet imprimé, il est important d’entretenir un bon rapport avec l’imprimeur. C’est lui qui aura la main sur les machines et donc le rendu final de vos productions, avoir un bon dialogue avec lui est donc indispensable. Notamment, si l’on veut pouvoir agir sur certains paramètres ou faire quelques essais tel que le choix d’un papier particulier. De plus, si on reprend l’exemple de le revue la Perruque, l’impression en amalgame inclut forcément un aspect relationnel. 
<p class="subdivision">C’est pourquoi, pour entretenir un bon rapport avec cet acteur de la chaîne graphique, une proximité est à valoriser tant pour l’aspect relationnel que l’aspect écologique du point de vue de l’impact des transports.</p>
<p class="subdivision">Il a le savoir relatif à l’impression et à ses modèles de machines, c’est l’expert de cette partie de la production, il est nécéssaire de le prendre en tant que tel et non pas juste comme un exécutant.</p>


