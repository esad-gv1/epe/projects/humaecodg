import { paginate } from "./js/paginate.js";
import { smoothScroll } from "./js/utils.js";

const closeBt = document.getElementById("closeBt");
closeBt.addEventListener("click", (event) => {
    window.parent.postMessage("closeMe");
});

let scrollMemory = {};
let recordScroll = true;

const styles = [
    "vendors/css/paged-preview.css",
    "css/print/print.css",
];

//message reception
window.addEventListener("message", async (event) => {
    console.log("message", event.source, event.data);

    if (event.data) {

        recordScroll = false;
        //Désérialiser les données
        const serializedElements = JSON.parse(event.data);
        //console.log("serializedElements", serializedElements)
        //Reconstruire les éléments HTML
        const elements = serializedElements.map(htmlString => {
            const tempDiv = document.createElement('div');
            tempDiv.innerHTML = htmlString;
            return tempDiv.firstChild;
        });

        const fakeBody = document.createElement("div");
        fakeBody.className="section";
        elements.forEach(element => {
            fakeBody.appendChild(element);
        });

        //pagination with Paged.js
        // await paginate(elements, styles);
        await paginate([fakeBody], styles);
        //get back previous UI elements
        document.body.appendChild(closeBt);
       
        //scroll
        setTimeout(() => {
            recordScroll = true;
            smoothScroll(scrollMemory.x, scrollMemory.y, 200);
        }, 100);
    }
});

window.addEventListener("beforeprint", (event) => {
    console.log("beforeprint");
    closeBt.style.display = "none";
});

window.addEventListener("afterprint", (event) => {
    console.log("afterprint");
    closeBt.style.display = "block";
});

//window.parent.postMessage({});

window.addEventListener("scroll", (event) => {
    if (recordScroll) {
        scrollMemory = { x: window.scrollX, y: window.scrollY };
        //console.log(scrollMemory);
    }
});