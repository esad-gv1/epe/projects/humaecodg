# Traitement des images


## Poids

Les images représentent en moyenne 45% du poids d’une page web. Sur le site httpsarchive.org on peut facilement constater que le nombres d’images présent sur le web a bien réduit depuis 2018 cependant on observe parallèlement une augmentation de leur poids. Il y a donc moins d’images mais ces dernières sont plus lourdes. 

## Dimensions
Préparer ses images aux dimensions adéquates permet de réduire leur poids quand ces dernières sont surdimensionnées. En effet, bien que l’on puisse agir sur les dimensions des images directement via le code, cela n’enlève en rien le poids et les dimensions du fichier source qui sera stocké, ni les efforts de calcul nécessaires à son chargement. Par exemple, une image dont les dimensions n’ont pas été adaptées peut avoir une taille entre 2000 et 4000 pixels, alors que l’affichage d’une page web se fera en moyenne entre 700 et 800 pixels, ce qui donne une image dont la taille fait plus du double de ce qui est utile. 

## Formats Webp et Avif
Les outils d’optimisation informatiques conseillent les formats Webp et Avif. 
Le format Webp a été développé en 2011 par Google et a fini par être intégré aux produits Apple. Il s’agit d’un fragment logiciel d’un conteneur plus complet. Ce format est en open source et disponible sur le site de Google developers pour les plus curieux et initiés. 
Développé et issu du milieu de la vidéo, le format Webp permet de réduire le poids d’une image par perte colorimétrique, spatiale, ou temporelle. Cela dit en agissant sur les paramètres de compression on peut faire en sorte que cette perte ne soit visible. 
Ce format permet de réduire la quantité de calcul du côté serveur et client et de réduire la charge, il s’agit d’un format de compression, hérité du format jpeg.
Le format AVIF, applique le même procédé, il a été développé par Netflix